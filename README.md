So in the base of our application lets say we want to assign a JWT when a user signs in.

First off we need to create a payload, what a payload is for JWT is all of that data that is going to be signed and stored into the base64 section of the token. 

- For example we can decode the JWT token, and can see it has data inside of it. [Example](https://i.imgur.com/pqPTjF8.png)

Lets say in this instance we want to store the users ID & Email.

```js
const payload = {
  // user would be from mongo (I forgot how you would get a collection in mongo)
  id: user.id,
  email: user.email
}
```

we would have
```js
const jwt = require("jsonwebtoken");

router.post("/login", async (req, res) => {
  // now all of our data is stored to the JWT
  const token = await jwt.sign({ payload }, "this-is-my-jwt-secret-so-ppl-cant-steal-the-data", { algorithm: 'HS256' });
  // this will now return the user token
  return res.send({ token: token });
}):
```

Now to use this token and retrieve user data from it, we will need something called a middleware, (this can give a little explanation on what middleware does if you're not familiar :smile: <https://expressjs.com/en/guide/using-middleware.html>

```js
const jwt = require("jsonwebtoken");

async function authorized(req, res, next) {
  let token;
  // we want to check our request headers for "Authorization" 
  if(!req.headers["Authorization"]) return res.status(401).send({ error: "Invalid auth data was provided.");

  // then we can validate the incoming request data to see if the user data is there
  await jwt.verify(token, "this-is-my-jwt-secret-so-ppl-cant-steal-the-data", async(err, data) => {
     const user = get.the.user.from.mongo({ where: data.id });
     if(!user) return res.status(401).send({ error: "No user was found with that ID" });
     if(err) return res.status(401).send({ error: "Invalid auth data was provided.");
     req.user = data;
     next();
  });
}
```

# Now on routes to check for JWT data to validate users and get data back we can do

Say you want to get data back on a `/users/me` route

```js
// authorized is our function from earlier, we just call it like this
app.get("/user/", authorized, async (req, res) => {
  // this reason we can do this is becasue we extended request and added the "user" attribute. "payload" also comes from our "payload" from earlier.  
  const user = mongodb.collection('users').findOne({ user: req.user.payload.id });
  return res.send({ user: `Hello my ID is ${user.name}` });
});
```

So when you sign in for example, you would get the token from the login response, then store that token in your localStorage, then from there you every time you have an authorized route that needs your user data, you would include your token from localStorage into your headers, so
```js
const token = localStorage.getItem("token");
axios.get("http://localhost:1337/something", {
    headers: {
      Authorization: `Bearer ${token}`
    }
  }
});
```

```js
const axios = require('axios');

const getData = async (username, password) => {
  const api = await axios.post("/login", { username: username, password: password });
  const res = api.json();
  return window.localStorage.setItem("token", res.data.token);
});

// idk if this would work idk how to do normal html requests thingy haha
<button onclick={username, password} />
```
